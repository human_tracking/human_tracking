# -*- coding: utf-8 -*-
"""
@author: ullu_da_pattha

Created by SKS, HBL Power Systems, Indian Railways
Date - 05/07/2015

@author: ullu_da_pattha

"""

# import the necessary packages
from __future__ import print_function
from imutils.object_detection import non_max_suppression
import numpy as np
import cv2
import imutils
 
# construct the argument parse and parse the arguments
 
# initialize the HOG descriptor/person detector
camera = cv2.VideoCapture(0)
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

	# load the image and resize it to (1) reduce detection time
	# and (2) improve detection accurac
camera.set(3,480)
camera.set(4,320)
while True:
    # Capture frame-by-frame
    ret, image = camera.read()
    #image = imutils.resize(image, width=min(400, image.shape[1]))
    (rects, weights) = hog.detectMultiScale(image, winStride=(4, 4), padding=(8, 8), scale=1.05)
    rects = np.array([[x,y,x+w,y+h] for (x,y,w,h) in rects])

    pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

    for (xa, ya, xb, yb) in pick:
    	cv2.rectangle(image, (xa, ya), (xb, yb), (0,255,0),2)
    

    cv2.imshow("After NMS", image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
    	break

# When everything is done, release the capture
camera.release()
cv2.destroyAllWindows()
