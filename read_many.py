#!/usr/bin/python

from __future__ import print_function
from os import listdir
from os.path import isfile, join
import numpy
import cv2
from imutils.object_detection import non_max_suppression
import numpy as np
import imutils

hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

mypath='/home/haraami/human_tracking/vlc_snaps_11nov/'
onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
image = numpy.empty(len(onlyfiles), dtype=object)
orig = numpy.empty(len(onlyfiles), dtype=object)


for n in range(0, len(onlyfiles)):
	image[n] = cv2.imread( join(mypath,onlyfiles[n]) )
	image[n] = imutils.resize(image[n], width=min(400, image[n].shape[1]))
	orig[n] = image[n].copy()

	(rects, weights) = hog.detectMultiScale(image[n], winStride=(4, 4),
	padding=(8, 8), scale=1.05)

	for (x, y, w, h) in rects:
		cv2.rectangle(orig[n], (x, y), (x + w, y + h), (0, 0, 255), 2)

	
	rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
	pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

	for (xA, yA, xB, yB) in pick:
		cv2.rectangle(image[n], (xA, yA), (xB, yB), (0, 255, 0), 2)

	cv2.imshow("Before NMS", orig[n])
	cv2.imshow("After NMS", image[n])
	cv2.waitKey(500)



