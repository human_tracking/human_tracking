import cv2
import os
import numpy as np
from PIL import Image
from scipy import misc
path = './sks_test'
recognizer = cv2.createLBPHFaceRecognizer()

image_paths = [os.path.join(path, f) for f in os.listdir(path) if not f.endswith ('.sad.jpg')]
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
for image_path in image_paths:
	print image_path
	print "\n"

images = []
    # labels will contains the label that is assigned to the image
labels = [1,2,3,4,5,6,7,8,9,10]
for image_path in image_paths:
    # Read the image and convert to grayscale
    image_pil = Image.open(image_path).convert('L')
    image_pil_resized = misc.imresize(image_pil,0.4,interp='bilinear',mode=None)
    cv2.imshow("resized image", image_pil_resized)
    cv2.waitKey(50)
    # Convert the image format into numpy array
    image = np.array(image_pil_resized, 'uint8')
    # Get the label of the image
    nbr = os.path.split(image_path)
    # Detect the face in the image
    print nbr
    faces = faceCascade.detectMultiScale(image)
    # If face is detected, append the face to images and the label to labels
    for (x, y, w, h) in faces:
        images.append(image[y: y + h, x: x + w])
        labels.append(nbr)
        cv2.imshow("Adding faces to traning set...", image[y: y + h, x: x + w])
        cv2.waitKey(50)

cv2.destroyAllWindows()


recognizer.train(image, np.array(labels))

image_path_sad = './sks_test/sks.sad.jpg'
predict_image_pil = Image.open(image_path_sad).convert('L')
predict_image = np.array(predict_image_pil, 'uint8')
faces = faceCascade.detectMultiScale(predict_image)
for (x, y, w, h) in faces:
    nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
    nbr_actual = os.path.split(image_path)
    if nbr_actual == nbr_predicted:
        print "{} is Correctly Recognized with confidence {}".format("sks", conf)
    else:
        print "{} is Incorrect Recognized as {}".format("pata nai kon h", nbr_predicted)
    cv2.imshow("Recognizing Face", predict_image[y: y + h, x: x + w])
    cv2.waitKey(1000)